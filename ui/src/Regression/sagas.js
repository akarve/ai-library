import axios from "axios"
import { call, put, takeLatest } from 'redux-saga/effects';
import { createAxiosErrorNotification } from "../Notifications";
import {
  CREATE_REGRESSION_PREDICTION,
  createRegressionPredictionPending,
  createRegressionPredictionFulfilled,
  createRegressionPredictionRejected
} from "./actions";

export const REGRESSOR_API_PATH = "/api/regressor/predictions";
export const REGRESSOR_MODEL_PATH = "healthpredictor/model.pkl";

let baseUrl = "http://host:port/";
if (window) {
  baseUrl = `${window.location.protocol}//${window.location.host}`;
}

export const REGRESSOR_API_FULL_URL = new URL(REGRESSOR_API_PATH, baseUrl).href;

export function createRegressionPredictionBody(height, weight) {
  return {"strData": `model=${REGRESSOR_MODEL_PATH}, data=${height}:${weight}`};
}

function* create(action) {
  let apiUrl = REGRESSOR_API_PATH;
  if (process.env.NODE_ENV === "development" && process.env.REACT_APP_DEV_REGRESSOR_API_URL) {
    apiUrl = process.env.REACT_APP_DEV_REGRESSOR_API_URL;
  }

  yield put(createRegressionPredictionPending(action.payload.height, action.payload.weight));
  try {
    const response = yield call(axios.post, apiUrl, createRegressionPredictionBody(action.payload.height, action.payload.weight));
    yield put(createRegressionPredictionFulfilled(response));
  } catch (error) {
    yield put(createAxiosErrorNotification(error));
    yield put(createRegressionPredictionRejected(error));
  }
}

export function* watchCreateRegressionPrediction() {
  yield takeLatest(CREATE_REGRESSION_PREDICTION, create);
}

export default [
  watchCreateRegressionPrediction()
];

