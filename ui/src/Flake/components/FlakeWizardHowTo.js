import * as React from "react";
import { Grid } from "patternfly-react";

function flakeWizardHowTo() {
  return (
    <div className="flake-wizard-contents flake-how-to">
      <Grid fluid={true}>
        <Grid.Row className="flake-info-section">
          <Grid.Col md={12}>
            <h1>Deploy Your Own</h1>
          </Grid.Col>
        </Grid.Row>
      </Grid>
    </div>);
}

export default flakeWizardHowTo;
