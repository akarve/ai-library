import axios from "axios";
import { call, put, takeLatest } from 'redux-saga/effects';
import { createAxiosErrorNotification } from "../Notifications";
import {
  CREATE_FLAKE_PREDICTION_LIST,
  createFlakePredictionListPending,
  createFlakePredictionsFulfilled,
  createFlakePredictionListItemPending,
  createFlakePredictionListItemFulfilled,
  createFlakePredictionListItemRejected,
  CREATE_CUSTOM_FLAKE_PREDICTION,
  createCustomFlakePredictionPending,
  createCustomFlakePredictionFulfilled,
  createCustomFlakePredictionRejected
} from "./actions";

export const FLAKE_API_PATH = "/api/flake/predictions";

let baseUrl = "http://host:port/";
if (window) {
  baseUrl = `${window.location.protocol}//${window.location.host}`;
}

export const FLAKE_API_FULL_URL = new URL(FLAKE_API_PATH, baseUrl).href;
export const FLAKE_MODEL_PATH = "cchase/flake-old/models/testflakes.model";

export function createFlakePredictionBody(log) {
  return {"strData": `model=${FLAKE_MODEL_PATH}, data=${log}`};
}

function* createPredictionListItem(index, log) {
  let apiUrl = FLAKE_API_PATH;
  if (process.env.NODE_ENV === "development" && process.env.REACT_APP_DEV_FLAKE_API_URL) {
    apiUrl = process.env.REACT_APP_DEV_FLAKE_API_URL;
  }

  yield put(createFlakePredictionListItemPending(index, log));
  try {
    const response = yield call(axios.post, apiUrl, createFlakePredictionBody(log));
    yield put(createFlakePredictionListItemFulfilled(index, response));
  } catch (error) {
    yield put(createAxiosErrorNotification(error));
    yield put(createFlakePredictionListItemRejected(index, error));
  }
}

function* createPredictionList(action) {
  const logList = action.payload.logList;

  const predictionList = logList.map(log => ({
    log,
    flake: null,
    predictionPending: false,
    predictionResponse: null,
    predictionError: null
  }));

  yield put(createFlakePredictionListPending(predictionList));
  for (const [index, log] of logList.entries()) {
    yield* createPredictionListItem(index, log);
  }
  yield put(createFlakePredictionsFulfilled());
}

export function* watchCreateFlakePredictionList() {
  yield takeLatest(CREATE_FLAKE_PREDICTION_LIST, createPredictionList);
}

function* createCustomPrediction(action) {
  let apiUrl = FLAKE_API_PATH;
  if (process.env.NODE_ENV === "development" && process.env.REACT_APP_DEV_FLAKE_API_URL) {
    apiUrl = process.env.REACT_APP_DEV_FLAKE_API_URL;
  }

  const log = action.payload.log;
  yield put(createCustomFlakePredictionPending(log));
  try {
    const response = yield call(axios.post, apiUrl, createFlakePredictionBody(log));
    yield put(createCustomFlakePredictionFulfilled(response));
  } catch (error) {
    yield put(createAxiosErrorNotification(error));
    yield put(createCustomFlakePredictionRejected(error));
  }
}

export function* watchCreateCustomPrediction() {
  yield takeLatest(CREATE_CUSTOM_FLAKE_PREDICTION, createCustomPrediction);
}

export default [
  watchCreateFlakePredictionList(),
  watchCreateCustomPrediction()
];
