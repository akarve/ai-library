from rad import rad
import logging
logging.disable(logging.WARN)
import pandas as pd
import numpy as np
import pandas as pd
import ipywidgets as widgets
from ipywidgets import interact, interactive
from IPython.display import display
import matplotlib.pyplot as plt
import s3fs
from datetime import datetime
import argparse
import os
import inspect
import sys
currentdir = os.path.dirname(
               os.path.abspath(
                inspect.getfile(inspect.currentframe())
                )
               )
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir + "/storage")
import s3
import datetime
from sklearn.externals import joblib
import tempfile

class detect_anomaly(object):
 
    def __init__(self):
        print("Initializing")

    def predict(self,data,features_names):

        result = "PASS"
        params = dict((item.strip()).split("=") for item in data.split(","))
        print(params)
        eparams = ["trainingdata", "resultlocation"]
        if not all (x in params for x in eparams):
          print("Not all parameters have been defined")
          result = "FAIL"
          return result

        s3Path = params['trainingdata']
        s3Destination = params['resultlocation']
        s3endpointUrl = os.environ['S3ENDPOINTURL']
        s3objectStoreLocation = os.environ['S3OBJECTSTORELOCATION']
        s3accessKey = os.environ['S3ACCESSKEY']
        s3secretKey = os.environ['S3SECRETKEY']
        s3Destination = params['resultlocation']

        tmpdir = str(tempfile.mkdtemp())
        dataurl = s3Path.split("/")
        DATA = dataurl[-1]
        FILE = tmpdir + "/" + DATA
        print(tmpdir)

        # Download input data from storage backend in to /tmp/ folder
        session = s3.create_session_and_resource(s3accessKey,
                                                 s3secretKey,
                                                 s3endpointUrl)
        s3.download_file(session,
                         s3objectStoreLocation,
                         s3Path,
                         FILE)

        if DATA.endswith('.csv'):
           frame = pd.read_csv(FILE)
           frame.shape
        elif DATA.endswith('.parquet'):
           frame = pd.read_parquet(FILE)

        frame_clean, mapping = rad.preprocess(frame,
                                              index=["account", "system_id"],
                                              drop="upload_time")

        # `preprocess` shaves-away some colums
        frame_clean.shape 

        frame.shape
        try: 
         forest = rad.RADIsolationForest(behaviour="new", contamination=0.1, n_estimators=150) 
         print(datetime.datetime.now())
         predictions = forest.fit_predict_contrast(frame_clean, training_frame=frame_clean) 
         print(datetime.datetime.now())
         filename = tmpdir + "/" + 'model.pkl'
         joblib.dump(predictions, filename)
        except Exception as e:
         print("Exception : " + e)


        # get all the anomalies and model as a pandas DataFrame
        anomalies = list(filter(lambda x: "anomalous_features" in x, predictions))
        frame_anomalies = pd.DataFrame.from_records(anomalies).set_index("system_id")

 	# join the anomalous records to the entire dataset
        account_and_system_id = pd.DataFrame.from_records(anomalies).set_index(["account", "system_id"])
        joined = frame_clean.join(account_and_system_id)
        joined["is_anomalous"] = joined["is_anomalous"].fillna(False)

        # aggregate values based on whether they were enriched or not
        agg = joined.groupby("is_anomalous").mean().fillna(1).T

        # compute the log-2 fold change ro help gauge magnitude of enrichment
        values = np.log2((agg[True] / agg[False])).sort_values(ascending=False).dropna()
        values.plot("bar")
        plt.ylabel("log-2 fold change") 
        plt.savefig(tmpdir + "/" + 'plot.png')

        predictions = "None"

        # Store model and plot to Ceph backend
        s3.upload_file(session,
                       s3objectStoreLocation,
                       tmpdir + "/" + 'model.pkl',
                       s3Destination+"/model.pkl")

        s3.upload_file(session,
                       s3objectStoreLocation,
                       tmpdir + "/" + 'plot.png',
                       s3Destination+"/plot.png")

        return predictions

def main():
  parser = argparse.ArgumentParser()
  parser.add_argument('-data', help='prediction data set', default='')
  args = parser.parse_args()
  data = args.data
  obj = detect_anomaly()
  obj.predict(data,20)
  
if __name__== "__main__":
  main()

