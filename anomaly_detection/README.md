# Anomaly Detection

Anomaly Detection is used to anomalous records in data. The model is based on outlier detection using Isolation Forest.

## Contents

`detect_anomaly.py` - IsolationForest model to identify anomalies.
##### Parameters
* trainingdata - location of training data in s3 backend
* resultlocation - location to upload trained model and plots

## Sample Query

curl -v http://detectanomaly-ai-library.10.16.208.2.nip.io/api/v0.1/predictions -d '{"strData":"trainingdata=aiops/meminfo.csv, resultlocation=aiops"}' -H "Content-Type: application/json"

